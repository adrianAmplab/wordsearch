import React from "react";
import PM_Logo from "../assets/images/PM-logo.png";
import PM_Coin_Gold from "../assets/images/PM-coin-gold.png";
import Arrow_Red_Down from "../assets/images/Arrow-red-down.png";
import "./Header.css";
import "../App.css";

class Header extends React.Component {
  render() {
    return (
      <header>
        <nav className="navbar">
          <a className="navbar-brand" href="#">
            <img src={PM_Logo} />
          </a>
          <div className="header-usercoin">
            <span className="user">
              <p>Welcome Username!</p>
              <span className="coin">
                <img src={PM_Coin_Gold}></img>
                <span id="coin">1234</span>
              </span>
            </span>
            <span className="arrow">
              <img src={Arrow_Red_Down}></img>
            </span>
          </div>
        </nav>
      </header>
    );
  }
}

export default Header;
