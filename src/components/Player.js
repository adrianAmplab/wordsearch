import React from "react";

class Player extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      play: false,
      audioFile: new Audio(props.url),
    };
  }
  togglePlay() {
    let test = this.state.audioFile;
    var playPromise = test.play();
    if (playPromise !== undefined) {
      playPromise
        .then(() => {
          console.log("playing");
        })
        .catch((err) => {
          console.log(err);
        });
    }
    // this.setState({ play: !this.state.play }, () => {
    //   this.state.play ? test.play() : test.pause();
    // });
  }
  render() {
    return (
      <div>
        <button onClick={this.togglePlay.bind(this)}>
          {this.state.play ? "Pause" : "Play"}
        </button>
      </div>
    );
  }
}
export default Player;
