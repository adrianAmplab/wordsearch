import React from "react";
import ReactDOM from "react-dom";
import "bootstrap/dist/css/bootstrap.min.css";
import "bootstrap/dist/js/bootstrap";
import $ from "jquery";
import App from "../App";
import "./Landing.css";
import bgLoading from "../assets/images/bg-loading.png";
import step1 from "../assets/images/img_step1.png";
import pmCoin from "../assets/images/icon_pmcoin.png";
import step1ws from "../assets/images/wordsearch-assets/img_step1.jpg";
import queryString from "query-string";

class Landing extends React.Component {
  constructor(props) {
    super(props);
    this.state = {};
  }
  handleLoadWS() {
    const windowUrl = window.location.search;
    const values = queryString.parse(windowUrl);
    console.log(values.variations);
    const intParam = parseInt(values.variations);
    if (
      parseInt(values.variations) > 7 ||
      values.variations == undefined ||
      isNaN(intParam)
    ) {
      values.variations = 0;
    }
    console.log(values.variations);
    ReactDOM.render(
      <React.StrictMode>
        <App variations={values.variations} />
      </React.StrictMode>,
      document.getElementById("root")
    );
  }
  render() {
    return (
      <main>
        <div className="container d-flex align-items-center">
          <div className="row">
            <div className="col-md-12 title">
              <h1>
                {" "}
                <img src={bgLoading} /> PM Digital Games
              </h1>
            </div>
            <div className="col-md-6 align-self-center">
              <div className="div-content">
                <img src={step1} className="findpm--step-1" />
                <div className="div--footer">
                  <h5>FIND PHILIP MORRIS</h5>
                  <p>Play now</p>
                  <a className="btn btn-default btn-block" href="/">
                    EARN <img src={pmCoin} className="coin" /> 50
                  </a>
                </div>
              </div>
            </div>

            <div className="col-md-6 align-self-center">
              <div className="div-content">
                <img src={step1ws} className="findpm--step-1" />
                <div className="div--footer">
                  <h5>WORD SEARCH</h5>
                  <p>Play now</p>
                  <a
                    className="btn btn-default btn-block"
                    onClick={this.handleLoadWS.bind(this)}
                  >
                    EARN <img src={pmCoin} className="coin" /> 50
                  </a>
                </div>
              </div>
            </div>
          </div>
        </div>
      </main>
    );
  }
}

export default Landing;
