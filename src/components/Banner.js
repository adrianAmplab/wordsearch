import React from "react";
import PM_Coin_Gold from "../assets/images/PM-coin-gold.png";
import Wifi from "../assets/images/wifi.png";
import "bootstrap/dist/css/bootstrap.min.css";
import "bootstrap/dist/js/bootstrap";
import $ from "jquery";
import "../App.css";
import "./Banner.css";
import gameBg from "../assets/audio/bg-music.ogg";

class Banner extends React.Component {
  constructor(props) {
    super(props);
    this.state = {};
  }
  componentDidMount() {
    // let music = document.getElementById("bg-music");
    // this.setState({ music: music, isPlaying: false });
    this.handleBgMusic();
  }
  handleBgMusic() {
    $(".unmute-bg-audio").on("click", function () {
      console.log("UNMUTE_INFO", $(".bg-music").prop("muted"));
      var playPromise = document.getElementById("audioBg").play();
      var audioBgSrc = document.getElementById("audioBg").src;
      console.log(playPromise);
      console.log("src", audioBgSrc);
      playPromise
        .then(() => {
          $(".bg-music").prop("muted")
            ? console.log("Paused")
            : console.log("Playing");
        })
        .catch((err) => {
          console.log(err);
        });
      if ($(".bg-music").prop("muted")) {
        $(".bg-music").prop("muted", false);
        localStorage.setItem("isMuted", false);
        $(".unmute-bg-audio")
          .html(`<svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-volume-down-fill" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
        <path fill-rule="evenodd" d="M8.717 3.55A.5.5 0 0 1 9 4v8a.5.5 0 0 1-.812.39L5.825 10.5H3.5A.5.5 0 0 1 3 10V6a.5.5 0 0 1 .5-.5h2.325l2.363-1.89a.5.5 0 0 1 .529-.06z"/>
        <path d="M10.707 11.182A4.486 4.486 0 0 0 12.025 8a4.486 4.486 0 0 0-1.318-3.182L10 5.525A3.489 3.489 0 0 1 11.025 8c0 .966-.392 1.841-1.025 2.475l.707.707z"/>
        </svg>`);
      } else {
        $(".bg-music").prop("muted", true);
        localStorage.setItem("isMuted", true);
        $(".unmute-bg-audio")
          .html(`<svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-volume-mute-fill" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
        <path fill-rule="evenodd" d="M6.717 3.55A.5.5 0 0 1 7 4v8a.5.5 0 0 1-.812.39L3.825 10.5H1.5A.5.5 0 0 1 1 10V6a.5.5 0 0 1 .5-.5h2.325l2.363-1.89a.5.5 0 0 1 .529-.06zm7.137 2.096a.5.5 0 0 1 0 .708l-4 4a.5.5 0 0 1-.708-.708l4-4a.5.5 0 0 1 .708 0z"/>
        <path fill-rule="evenodd" d="M9.146 5.646a.5.5 0 0 0 0 .708l4 4a.5.5 0 0 0 .708-.708l-4-4a.5.5 0 0 0-.708 0z"/>
        </svg>`);
      }
    });
    // if (this.state.music.paused) {
    //   $(".unmute-bg-audio")
    //     .html(`<svg width="1em" height="1em" viewBox="0 0 16 16" className="bi bi-volume-down-fill" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
    //     <path fill-rule="evenodd" d="M8.717 3.55A.5.5 0 0 1 9 4v8a.5.5 0 0 1-.812.39L5.825 10.5H3.5A.5.5 0 0 1 3 10V6a.5.5 0 0 1 .5-.5h2.325l2.363-1.89a.5.5 0 0 1 .529-.06z"/>
    //     <path d="M10.707 11.182A4.486 4.486 0 0 0 12.025 8a4.486 4.486 0 0 0-1.318-3.182L10 5.525A3.489 3.489 0 0 1 11.025 8c0 .966-.392 1.841-1.025 2.475l.707.707z"/>
    //     </svg>`);
    //   this.setState({ isPlaying: !this.state.music.paused });
    //   return this.state.music.play();
    // } else {
    //   $(".unmute-bg-audio")
    //     .html(`<svg width="1em" height="1em" viewBox="0 0 16 16" className="bi bi-volume-mute-fill" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
    //     <path fill-rule="evenodd" d="M6.717 3.55A.5.5 0 0 1 7 4v8a.5.5 0 0 1-.812.39L3.825 10.5H1.5A.5.5 0 0 1 1 10V6a.5.5 0 0 1 .5-.5h2.325l2.363-1.89a.5.5 0 0 1 .529-.06zm7.137 2.096a.5.5 0 0 1 0 .708l-4 4a.5.5 0 0 1-.708-.708l4-4a.5.5 0 0 1 .708 0z"/>
    //     <path fill-rule="evenodd" d="M9.146 5.646a.5.5 0 0 0 0 .708l4 4a.5.5 0 0 0 .708-.708l-4-4a.5.5 0 0 0-.708 0z"/>
    //     </svg>`);
    //   this.setState({ isPlaying: !this.state.music.paused });
    //   return this.state.music.pause();
    // }
  }
  render() {
    return (
      <div className="Banner">
        <audio
          hidden="true"
          className="bg-music"
          name="media"
          preload="auto"
          src={gameBg}
          muted
          crossOrigin="anonymous"
          id="audioBg"
          type="audio/ogg"
        >
          {/* <source src="/gamebg.mp3" type="audio/mpeg" /> */}
          {/* <source src={gameBg} type="audio/mpeg" /> */}
          {/* <source src="/bg-music.ogg" type="audio/mpeg" /> */}
        </audio>
        <h1>
          WORD SEARCH{" "}
          <button
            className="btn btn-wifi"
            onClick={() => {
              $(".wifi-message").toggle();
            }}
          >
            {" "}
            ?
          </button>
          <a className="volume unmute-bg-audio">
            <svg
              width="1em"
              height="1em"
              viewBox="0 0 16 16"
              className="bi bi-volume-mute-fill"
              fill="currentColor"
              xmlns="http://www.w3.org/2000/svg"
            >
              <path
                fill-rule="evenodd"
                d="M6.717 3.55A.5.5 0 0 1 7 4v8a.5.5 0 0 1-.812.39L3.825 10.5H1.5A.5.5 0 0 1 1 10V6a.5.5 0 0 1 .5-.5h2.325l2.363-1.89a.5.5 0 0 1 .529-.06zm7.137 2.096a.5.5 0 0 1 0 .708l-4 4a.5.5 0 0 1-.708-.708l4-4a.5.5 0 0 1 .708 0z"
              />
              <path
                fill-rule="evenodd"
                d="M9.146 5.646a.5.5 0 0 0 0 .708l4 4a.5.5 0 0 0 .708-.708l-4-4a.5.5 0 0 0-.708 0z"
              />
            </svg>
          </a>
        </h1>
        <p>
          Level up your coins now! Accomplish this task to earn{" "}
          <img src={PM_Coin_Gold} />
          <span> 50</span>
        </p>
        <div className="wifi-message">
          <div className="alertwifi">
            <img src={Wifi} />
            <p>
              Make sure your Internet connection <br /> is stable before you
              start the game.
            </p>
            <button
              className="btn btn-default btn-close wifi-close"
              onClick={() => {
                $(".wifi-message").toggle();
              }}
            >
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
        </div>
      </div>
    );
  }
}

export default Banner;
