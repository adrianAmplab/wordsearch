import React from "react";
import "./ConfirmExit.css";
import btnClose from "../assets/images/btn-top-close.png";
import $ from "jquery";

class ConfirmExit extends React.Component {
  handleClose() {
    console.log("handleClose");
    $(".confirmExit_outer").toggle();
  }
  handleExit() {
    window.location.reload();
  }
  render() {
    return (
      <div className="confirmExit_outer">
        <div className="confirmExit_inner">
          <div className="top-buttons">
            <button className="btn top-buttons btn-close">
              <img
                className="img-close"
                src={btnClose}
                onClick={() => this.handleClose()}
              />
            </button>
          </div>
          <h3>
            Are you sure you want
            <br />
            to end the game
          </h3>
          <p>
            You can continue playing
            <br />
            to earn coins
          </p>
          <a
            className="btn red-button btn-block confirmBtns btn-animate"
            id="confirmNo"
            onClick={() => this.handleClose()}
          >
            {" "}
            NO{" "}
          </a>
          <a
            className="btn red-button btn-block confirmBtns"
            id="confirmYes"
            onClick={() => this.handleExit()}
          >
            {" "}
            YES{" "}
          </a>
        </div>
      </div>
    );
  }
}

export default ConfirmExit;
