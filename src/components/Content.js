import React from "react";
import "bootstrap/dist/css/bootstrap.min.css";
import "bootstrap/dist/js/bootstrap";
import $ from "jquery";
import Splash from "../assets/images/WordSearch2.jpg";
import WordSearchMechanics1 from "../assets/images/WordSearchMechanics1.jpg";
import WordSearchMechanics2 from "../assets/images/WordSearchMechanics2.png";
import btnBack from "../assets/images/btn-top-back.png";
import btnClose from "../assets/images/btn-top-close.png";
import "./Content.css";
import ws from "../assets/js/wordsearch.js";

import ConfirmExit from "../components/ConfirmExit";

class Content extends React.Component {
  constructor() {
    super();
    this.state = {
      firstWord: "",
      hintArray: [],
      toggleHint: false,
    };
  }
  handleNext(i) {
    const page = parseInt(i);

    $("div.Page" + (page - 1)).css({ display: "none" });
    $("div.Page" + (page - 2)).css({ display: "none" });
    $("div.Page" + page).css({ display: "block" });
    this.showHomeButton(page);
    this.adjustRedBox(page);
  }

  handleBack(i) {
    const page = parseInt(i);
    $("div.Page" + (page + 1)).css({ display: "none" });
    $("div.Page" + page).css({ display: "block" });
    this.showHomeButton(page);
    this.adjustRedBox(page);
  }

  showHomeButton(page) {
    if (parseInt(page) === 1) {
      $(".home-button").css({ display: "block" });
    } else {
      $(".home-button").css({ display: "none" });
    }
  }

  adjustRedBox(page) {
    if (parseInt(page) === 4) {
      $(".Content").css({
        "background-size": "100% 105px",
        "background-repeat": "no-repeat",
        "background-position": "top center",
      });
    } else {
      $(".Content").css({
        "background-size": "cover",
        "background-repeat": "repeat",
        "background-position": "center center",
      });
    }
  }
  handleExit() {
    $("div.Page" + 1).css({ display: "block" });
    $("div.Page" + 2).css({ display: "none" });
    $("div.Page" + 3).css({ display: "none" });
    $("div.Page" + 4).css({ display: "none" });
    this.showHomeButton(parseInt(1));
    this.adjustRedBox(parseInt(1));
  }
  handleConfirm(i) {
    console.log(i);
    const page = parseInt(i);
    $(`.confirmExit_outer`).toggle();
  }
  componentDidMount() {
    const WordSearch = require("../assets/js/wordsearch.js");
    var wordsWrap = $("#wordsList");
    var firstWord = "";
    this.props.words.forEach((word, i) => {
      var liEl = document.createElement("li");
      liEl.setAttribute("class", "ws-word circle");
      liEl.innerText = capitalizeFirstLetter(word);
      wordsWrap.append(liEl);

      if (i == 0) {
        firstWord = word;
      }
    });
    const wsarea_id = $("#ws-area").attr("id");
    let hintArray = ws(wsarea_id, { words: this.props.words });
    this.passParameters(firstWord, hintArray);
    console.log(this.state);
  }

  passParameters(firstWord, hintArray) {
    this.setState({
      firstWord: firstWord,
      hintArray: hintArray,
      toggleHint: false,
    });
  }

  toggleHint() {
    console.log(this.state);
    for (var i = 0; i < this.state.firstWord.length; i++) {
      var letter = this.state.hintArray[i];
      var row = letter[0];
      var col = letter[1];
      console.log("row", row);
      console.log("col", col);
      if (this.state.toggleHint) {
        console.log("remove");
        $(`[col="${col}"][row="${row}"]`).removeClass("hint");
      } else {
        console.log("add");
        $(`[col="${col}"][row="${row}"]`).addClass("hint");
      }
      this.setState({ toggleHint: !this.state.toggleHint });
    }
  }

  render() {
    return (
      <div className="Content">
        <Page1 onClick={() => this.handleNext(2)} />
        <Page2
          onClick={() => this.handleNext(4)}
          back={() => this.handleBack(1)}
          confirm={() => this.handleConfirm(2)}
        />
        <Page3
          onClick={() => this.handleNext(4)}
          back={() => this.handleBack(2)}
          confirm={() => this.handleConfirm(3)}
        />
        <Page4
          test={this.test}
          confirm={() => this.handleConfirm(2)}
          showHint={this.toggleHint.bind(this)}
        />
      </div>
    );
  }
}
function Page1(props) {
  return (
    <div className="Page1">
      <div className="splash"></div>
      <a
        className="btn red-button btn-block btn-animate"
        id="playNow"
        onClick={() => props.onClick()}
      >
        {" "}
        PLAY NOW{" "}
      </a>
      <p>
        <b>DISCLAIMER:</b> WORD SEARCH CAN ONLY BE PLAYED ONCE A DAY. ONE TRY
        ONLY.
      </p>
    </div>
  );
}
function Page2(props) {
  return (
    <div className="Page2">
      <div className="top-buttons">
        <button
          className="btn top-buttons btn-back"
          onClick={() => props.back()}
        >
          <img className="img-back" src={btnBack} />
        </button>
        <button className="btn top-buttons btn-close">
          <img
            className="img-close"
            src={btnClose}
            onClick={() => props.confirm()}
          />
        </button>
      </div>
      <div className="splash"></div>
      <h5>
        Level up your Philip Morris knowledge <br />
        by looking for the hidden words!
      </h5>
      <a
        className="btn red-button btn-block btn-animate"
        id="next1"
        onClick={() => props.onClick()}
      >
        {" "}
        START{" "}
      </a>
      <ConfirmExit />
    </div>
  );
}
function Page3(props) {
  return (
    <div className="Page3">
      <div className="top-buttons">
        <button
          className="btn top-buttons btn-back"
          onClick={() => props.back()}
        >
          <img className="img-back" src={btnBack} />
        </button>
        <button className="btn top-buttons btn-close">
          <img
            className="img-close"
            src={btnClose}
            onClick={() => props.confirm()}
          />
        </button>
      </div>
      <div className="splash"></div>
      <h5>
        Tap and drag horizontally, vertically, or diagonally
        <br />
        to select words. Find all 5 of them to win coins!
      </h5>
      <a
        className="btn red-button btn-block btn-animate"
        id="next2"
        onClick={() => props.onClick()}
      >
        {" "}
        START{" "}
      </a>
      <ConfirmExit />
    </div>
  );
}
function Page4(props) {
  return (
    <div className="Page4">
      <div className="top-buttons">
        <button className="btn top-buttons btn-close">
          <img
            className="img-close"
            src={btnClose}
            onClick={() => props.confirm()}
          />
        </button>
      </div>
      <div id="menu-outer">
        <div className="table">
          <h4>Look for</h4>
          <ul className="ws-words" id="wordsList"></ul>
        </div>
      </div>
      <div className="wrap">
        <section id="ws-area"></section>
      </div>
      <a
        className="btn red-button btn-block"
        id="hintBtn"
        onClick={() => props.showHint()}
      >
        {" "}
        HINT{" "}
      </a>
      <ConfirmExit />
    </div>
  );
}

function capitalizeFirstLetter(string) {
  return string.charAt(0).toUpperCase() + string.slice(1);
}
export default Content;
