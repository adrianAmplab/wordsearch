import React from "react";
import "bootstrap/dist/css/bootstrap.min.css";
import "bootstrap/dist/js/bootstrap";
import $ from "jquery";
import "./App.css";
import Header from "./components/Header";
import Footer from "./components/Footer";
import Content from "./components/Content";
import Banner from "./components/Banner";

class WordSearch extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      page: 1,
      status: "toStart",
      words: [
        ["crest", "path", "heritage", "firmstick", "thegentleman"],
        ["hot", "red", "sterling", "silver", "true", "gold"],
        ["goaloriented", "goodlooking", "status", "levelup"],
        ["firm", "filter", "tobacco", "extra", "consistent", "taste"],
        ["confident", "imaginative", "encouraging", "aspirational"],
        ["winning", "grounded", "discerning"],
        ["menthol", "red", "fresh", "flavor", "green"],
      ],
    };
    $("html").on("contextmenu", (e) => {
      return false;
    });
  }
  componentDidMount() {
    console.log("VARIATIONS", this.state.words[this.props.variations]);
    console.log(this.props.variations);
  }
  handleHome() {
    window.location.reload();
  }
  render() {
    return (
      <div className="App">
        <Banner />
        <Content words={this.state.words[this.props.variations]} />
        <a className="btn home-button" onClick={this.handleHome.bind(this)}>
          {" "}
          HOME{" "}
        </a>
        <Footer />
      </div>
    );
  }
}

export default WordSearch;
