const loadWS = (callback) => {
  const existingScript = document.getElementById("wordSearch");
  if (!existingScript) {
    const script = document.createElement("script");
    script.src = "./wordsearch.js";
    script.id = "wordSearch";
    document.body.appendChild(script);
    script.onload = () => {
      if (callback) callback();
    };
  }
  if (existingScript && callback) callback();
};
export default loadWS;
